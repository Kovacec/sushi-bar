const path = require('path')
const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {

    entry: './resources/app.js',
    output: {
        path: path.resolve(__dirname, "public"),
        publicPath: "/",
        filename: "bundle.js"
    },

    // TODO: hot reload not working on custom domain ; yarn run prod - will create right bundle.js
    devServer: {
        filename: 'bundle.js',
        hot: true,
        contentBase: path.resolve(__dirname, 'public'),
        publicPath: '/',
        allowedHosts: [
            'sushi-bar.local'
        ]
    },
    mode: process.env.NODE_ENV,
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'postcss-loader'
                ],
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ],
    },
    plugins: [

        new VueLoaderPlugin(),

    ],

}
