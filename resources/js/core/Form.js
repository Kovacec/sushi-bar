import Errors from './Errors'
import axios from 'axios'
import qs from 'qs'

class Form {
    /**
     * Create a new Form instance.
     *
     * @param {object} data
     */
    constructor(data, clearFields = true) {
        this.clearFields = clearFields
        this.originalData = data

        for (const field in data) {
            this[field] = data[field]
        }

        this.errors = new Errors()
        this.submitting = false
    }

    /**
     * Fetch all relevant data for the form.
     */
    data() {
        const data = {}

        for (const property in this.originalData) {
            data[property] = this[property]
        }

        return data
    }

    /**
     * Reset the form fields.
     */
    reset() {
        if (this.clearFields) {
            for (const field in this.originalData) {
                if (this[field] == true) {
                    this[field] = false
                } else {
                    this[field] = ''
                }
            }
        }
    }

    /**
     * Send a POST request to the given URL.
     * .
     * @param {string} url
     */
    post(url) {
        return this.submit('post', url)
    }

    /**
     * Send a PUT request to the given URL.
     * .
     * @param {string} url
     */
    put(url) {
        return this.submit('put', url)
    }

    /**
     * Send a PATCH request to the given URL.
     * @param {string} url
     */
    patch(url) {
        return this.submit('patch', url)
    }

    /**
     *  Send a DELETE request to the given URL.
     * @param {string} url
     */
    delete(url) {
        return this.submit('delete', url)
    }

    /**
     * Submit the form.
     * @param {string} requestType
     * @param {string} url
     */
    submit(requestType, url) {

        this.submitting = true

        return new Promise((resolve, reject) => {
            axios[requestType](url, qs.stringify(this.data()))
                .then(response => {
                    this.onSuccess(response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    console.log(error.response.data, 'catch ...')
                    this.onFail(error.response.data)
                    reject(error.response.data)
                })
        })
    }

    /**
     * Handle a successful form submission.
     *
     * @param {object} data
     */
    onSuccess(data) {
        this.submitting = false
        this.errors.clear()
    }

    /**
     * Handle a failed form submission.
     *
     * @param {object} errors
     */
    onFail(errors) {
        this.submitting = false
        console.log(errors, 'err')
        this.errors.record(errors)
    }
}

export default Form
