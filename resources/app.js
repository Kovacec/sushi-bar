import styles from './css/styles.css'

import Vue from 'vue';
import axios from 'axios';

import SushiBar from './js/components/SushiBar.vue'
import Seat from './js/components/Seat.vue'

Vue.component('suschi-bar', SushiBar)
Vue.component('seat', Seat)

const vue = new Vue({

    data: {
        radius: 500,
        defaultSeats: 12
    },

    methods: {
        
        setBarRadius: function () {

            let width = document.getElementById('bar').offsetWidth
            let height = document.getElementById('bar').offsetHeight

            let tmpRadius = (width < height) ? width : height;

            if (tmpRadius > 200 && tmpRadius < 600) {
                this.radius = tmpRadius
            }

        }
    },

    mounted() {
        this.setBarRadius()
        window.addEventListener('resize', this.setBarRadius)
    },

    beforeDestroy() {
        window.removeEventListener('resize', this.setBarRadius)
    }


}).$mount("#app");