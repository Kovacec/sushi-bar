<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DistributionController extends Controller
{
    /**
     * Return new distribution so that bigger empty group is better.
     *
     * @return json
     * @throws \Illuminate\Validation\ValidationException
     */
    public function biggestEmptyGroup(Request $request)
    {

        $this->validate($request, [
            'sizeOfNewGroup' => ['required', 'not_in:0', 'regex:/^\d+$/'],
            'seats' => 'required',
            'seats.*' => ["regex:(true|false)"]
        ]);

        // TODO: check why request does not cast boolean array anymore ('true'|'false')
        $seats = array_map(function($taken) {
            return filter_var($taken, FILTER_VALIDATE_BOOLEAN);
        }, $request->get('seats'));

        $points = -1;
        $bestDistribution = null;
        $positions = $this->possiblePositions($seats, $request->input('sizeOfNewGroup'));

        foreach ($positions as $possiblePositions)
        {
            $possibleDistribution = $this->setPosition($request->get('seats'), $possiblePositions);
            $tmpPoints = $this->eval($possibleDistribution);

            if ($tmpPoints > $points)
            {
                $points = $tmpPoints;
                $bestDistribution = $possibleDistribution;
            }

        }

        if ($bestDistribution === null) {
            return response()->json(['sizeOfNewGroup' => [
                'No seats found.'
            ]], 422);
        }

        return array_map(function($taken) {
            return filter_var($taken, FILTER_VALIDATE_BOOLEAN);
        }, $bestDistribution);
    }

    /**
     * Get all possible positions for given group.
     *
     * @return array
     */
    private function possiblePositions($seats, $sizeOfNewGroup)
    {
        $numberOfSeats = count($seats);
        $possiblePositions = [];

        for ($i = 0; $i < $numberOfSeats; $i++) {

            if ($seats[$i]) {
                continue;
            }

            $position = [];
            for ($j = $i; $j < $i + $sizeOfNewGroup; $j++) {
                if ($seats[$j % $numberOfSeats]) {
                    break;
                }

                $position[] = $j % $numberOfSeats;
            }

            if (count($position) == $sizeOfNewGroup) {
                $possiblePositions[] = $position;
            }

        }

        return $possiblePositions;
    }

    private function setPosition($seats, $position)
    {
        foreach ($position as $index) {
            $seats[$index] = true;
        }

        return $seats;
    }

    /**
     * Evaluate seats distribution.
     *
     * @param $seats
     * @return int
     */
    private function eval($seats)
    {
        $tmpPoints = 1;
        $points = 0;

        foreach ($seats as $taken) {
            if (!$taken) {
                $points += $tmpPoints;
                $tmpPoints += 1;
            } else {
                $tmpPoints = 1;
            }

        }

        return $points;
    }

}
